import numpy as np
import matplotlib.pyplot as plt
import cv2
from sklearn import cluster
from scipy.spatial.distance import pdist, squareform, cdist


class PQ:
    def __init__(self, clusters, db_index, n_clusters=256, m=8, D=128, N=0):
        self.n_clusters = n_clusters
        self.m = m
        self.clusters = clusters
        self.db_index = db_index
        self.D = D
        self.N = N

    def __repr__(self):
        isready = len(self.clusters) != 0 and len(self.db_index) != 0
        return "The database of {N} samples has been quantized in {m} subvectors, each subvector has been clustered " \
               "in {n_clusters} clusters, and it is {readyornot} for requesting the approximate nearest neighbour of " \
               "a {D}-dimensional vector.".format(N=self.N, m=self.m, n_clusters=self.n_clusters, readyornot="ready" if
        isready else "not ready", D=self.D)


def productquantize_this(descriptors, n_clusters, m):
    """
    Computes the Product Quantization of a database of descriptors
    :param descriptors: the database, shape N x D
    n_clusters : the number of clusters for the subvectors
    m : the number of subdivision
    :return: a PQ object containing the internal parameters of the quantization
    """

    # Splitting of the list of N D-dimensional descriptors (if SIFT D=128) to m D/m-dimensional subvectors
    # subarr is a list of m matrices of shape (N x D/m)
    subarr = np.split(descriptors, m, axis=1)

    N = subarr[0].shape[0]  #  Number of samples

    # Initializing the clustering task, where each subvector will be clustered in n_clusters clusters of dimension D/m
    KMeans = cluster.MiniBatchKMeans(n_clusters=n_clusters)
    clusters = np.zeros((m, n_clusters, subarr[0].shape[1]))

    for i in range(m):
        print("Clustering of subvector N°{num}".format(num=i))

        # In subarr each subvector is replaced by the index of the closest cluster
        subarr[i] = KMeans.fit_predict(subarr[i])
        # Storing of the cluster positions
        clusters[i, :, :] = KMeans.cluster_centers_

    PQobject = PQ(n_clusters=n_clusters, m=m, clusters=clusters, db_index=subarr, D=128, N=N)
    print(PQobject)

    return PQobject


def findNNfromPQ(request, PQobject):
    """
    finds the nearest neighbour of the request in the database quantized in PQobject
    :param request: request vector, must be of dimension D
    :param PQobject: PQ containing the parameters and the arrays with clusters and index for the vectors of the DB
    :return: the index of the nearest neighbour
    """

    # Splitting the request vector the same way we did before
    splitrequest = np.asarray(np.split(request, PQobject.m))

    # distancetable will store the distances between the request subvectors and the clusters of our database subvectors
    distancetable = np.zeros((PQobject.m, PQobject.n_clusters))

    # subdistances will store the distances between the request subvectors and the database subvectors represented by
    # their corresponding cluster
    subdistances = np.zeros((PQobject.N, PQobject.m))

    for i in range(PQobject.m):
        distancetable[i, :] = cdist(np.array([splitrequest[i, :]]), PQobject.clusters[i, :, :], 'sqeuclidean')
        subdistances[:, i] = distancetable[i, PQobject.db_index[i]]

    #  Sum of the distances across the m subdivision to obtain the approximated table of distances
    distances = np.sum(subdistances, axis=1)

    return np.argmin(distances)

def main():

    img = cv2.imread("notredameoblique.png")
    imggray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    SIFT1 = cv2.xfeatures2d.SIFT_create()
    det1, desc1 = SIFT1.detectAndCompute(imggray, None)

    testPQ = productquantize_this(desc1, 128, 8)

    print(findNNfromPQ(desc1[0], testPQ))


if __name__ == "__main__":
    main()
